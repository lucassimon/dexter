import sys

from apps.game import Game

if __name__ == "__main__":
    result = [0]
    game = Game()

    while len(result) > 0:
        game.play()
        result = game.result()

        if result:
            print(f"\n I suppose that these choices are wrong {result}")
        else:
            sys.exit(0)
