import pytest

from apps.places import Place


class TestPlaces:
    def setup_method(self):
        self.place = Place("some text")

    def test_should_return_the_gun_with_your_name(self):
        assert self.place.__str__() == "Place: some text"

    def test_guns_class_has_static_options(self):
        assert Place.OPTIONS == [
            "Redmond",
            "Palo Alto",
            "San Francisco",
            "Tokio",
            "Restaurante no Fim do Universo",
            "São Paulo",
            "Cupertino",
            "Helsinki",
            "Maida Vale",
            "Toronto",
        ]
