import pytest
from unittest.mock import patch

from apps.suspects import Suspect
from apps.questions import Question


class TestQuestion:
    def setup_method(self):
        items = [Suspect(suspect) for suspect in Suspect.OPTIONS]
        self.question = Question("some question?", items)

    def test_should_print_the_question_and_input(self):
        answer_input = "3"
        expected_answer = 3
        with patch("builtins.input", side_effect=answer_input):
            answer = self.question.do()

        assert answer == expected_answer
