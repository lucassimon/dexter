import pytest

from apps.suspects import Suspect


class TestSuspects:
    def setup_method(self):
        self.suspect = Suspect("some text")

    def test_should_return_the_gun_with_your_name(self):
        assert self.suspect.__str__() == "Suspect: some text"

    def test_guns_class_has_static_options(self):
        assert Suspect.OPTIONS == [
            "Charles B. Abbage",
            "Donald Duck Knuth",
            "Ada L. Ovelace",
            "Alan T. Uring",
            "Ivar J. Acobson",
            "Ras Mus Ler Dorf",
        ]
