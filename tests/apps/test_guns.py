import pytest

from apps.guns import Gun


class TestGuns:
    def setup_method(self):
        self.gun = Gun("some text")

    def test_should_return_the_gun_with_your_name(self):
        assert self.gun.__str__() == "Gun: some text"

    def test_guns_class_has_static_options(self):
        assert Gun.OPTIONS == [
            "Peixeira",
            "DynaTAC 8000X (o primeiro aparelho celular do mundo)",
            "Trezoitão",
            "Trebuchet",
            "Maça",
            "Gládio",
        ]
