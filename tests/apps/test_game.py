import pytest
from unittest.mock import patch

from apps.game import Game


class TestGame:
    def setup_method(self):
        self.game = Game()

    def test_should_return_true_when_the_answers_are_equal_to_valid_choices(self):
        correctly_answer_input = ["2", "4", "3"]
        with patch("builtins.input", side_effect=correctly_answer_input):
            self.game.play()

        assert self.game.solved_the_crime() is True

    def test_should_return_false_when_the_suspect_is_wrong(self):
        correctly_answer_input = ["99", "4", "3"]
        with patch("builtins.input", side_effect=correctly_answer_input):
            self.game.play()

        assert self.game.check_suspect() is False

    def test_should_return_false_when_the_place_is_wrong(self):
        correctly_answer_input = ["2", "99", "3"]
        with patch("builtins.input", side_effect=correctly_answer_input):
            self.game.play()

        assert self.game.check_place() is False

    def test_should_return_false_when_the_gun_is_wrong(self):
        correctly_answer_input = ["2", "4", "99"]
        with patch("builtins.input", side_effect=correctly_answer_input):
            self.game.play()

        assert self.game.check_gun() is False

    @pytest.mark.parametrize(
        "answer, expected",
        [
            ([2, 4, 3], []),
            ([1, 1, 1], [1, 2, 3]),
            ([3, 1, 3], [1, 2]),
            ([5, 4, 3], [1]),
            ([2, 4, 13], [3]),
        ],
    )
    def test_result(self, answer, expected):
        with patch("builtins.input", side_effect=answer):
            self.game.play()

        assert self.game.result() == expected
