class Question:
    def __init__(self, question, items):
        self.question = question
        self.items = items

    def do(self):
        print(f"\n {self.question}")

        for index, item in enumerate(self.items):
            print(f"\n [{index + 1}] - {item.name}")

        answer = int(input("Answer :"))

        return answer
