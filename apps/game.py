from operator import eq

from .guns import Gun
from .places import Place
from .suspects import Suspect
from .questions import Question


class Game:
    def __init__(self):
        self.valid = [2, 4, 3]
        self.suspects = [Suspect(suspect) for suspect in Suspect.OPTIONS]
        self.places = [Place(place) for place in Place.OPTIONS]
        self.guns = [Gun(gun) for gun in Gun.OPTIONS]
        self.question_1 = Question("Who is the suspect?", self.suspects)
        self.question_2 = Question("Where do you think he was?", self.places)
        self.question_3 = Question("How did he die?", self.guns)
        self.answers = []

    def play(self):
        # when play is called the answares need to be an empty list
        self.answers = []

        answer_1 = self.question_1.do()
        self.answers.append(answer_1)

        answer_2 = self.question_2.do()
        self.answers.append(answer_2)

        answer_3 = self.question_3.do()
        self.answers.append(answer_3)

    def solved_the_crime(self):
        return self.valid == self.answers

    def check_suspect(self):
        return self.valid[0] == self.answers[0]

    def check_place(self):
        return self.valid[1] == self.answers[1]

    def check_gun(self):
        return self.valid[2] == self.answers[2]

    def result(self):
        correct_answer = []
        if self.solved_the_crime():
            print("\n\n===== Wooooooow you solve this crime! ======")
            return correct_answer

        if not self.check_suspect():
            correct_answer.append(1)

        if not self.check_place():
            correct_answer.append(2)

        if not self.check_gun():
            correct_answer.append(3)

        return correct_answer
