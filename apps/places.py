class Place:
    OPTIONS = [
        "Redmond",
        "Palo Alto",
        "San Francisco",
        "Tokio",
        "Restaurante no Fim do Universo",
        "São Paulo",
        "Cupertino",
        "Helsinki",
        "Maida Vale",
        "Toronto",
    ]

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"Place: {self.name}"
