class Suspect:
    OPTIONS = [
        "Charles B. Abbage",
        "Donald Duck Knuth",
        "Ada L. Ovelace",
        "Alan T. Uring",
        "Ivar J. Acobson",
        "Ras Mus Ler Dorf",
    ]

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"Suspect: {self.name}"
