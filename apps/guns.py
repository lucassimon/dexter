class Gun:
    OPTIONS = [
        "Peixeira",
        "DynaTAC 8000X (o primeiro aparelho celular do mundo)",
        "Trezoitão",
        "Trebuchet",
        "Maça",
        "Gládio",
    ]

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"Gun: {self.name}"
