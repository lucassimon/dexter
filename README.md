# Dexter game


TO run test install the packages with `pip install -r requrements-dev.txt` and run `pytest`

To play the game, please, run the `python start.py`

The main libraries are in `apps` folder.

Thanks


## Improve

I can use random in valid variable when initialize the Game to do more expierence of the Dexter detective game. And on tests, I can mock the result of random to do the asserts correctly.

So, the player will catch the wrongs results (annotate it) to understand where he failed and, based on these results, solve the crime.

